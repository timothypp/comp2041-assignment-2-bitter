#!/usr/bin/perl -w

#
# Written by Timothy Putra Pringgondhani (z5043683)
# z5043683@zmail.unsw.edu.au
#
# Created on 22 October 2015
# Last Update on 25 October 2015
#

#
# This CGI file helps to display the action page appropriate to the action that the user wanted.
# This CGI will only displays the necessary action. If an action is not specified, it will
# redirect the user tot the home page.
#
# This CGI file does small checking whether the appropriate information is filled in and
# sanitizes any information passed in.
#

use strict;

use CGI qw/:all/;
use CGI::Cookie;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

use HTML;
use Security;

my %page_title = (
	'fp' => 'Recover Password',
	'fo' => 'Follow',
	'uf' => 'Unfollow',
	'rm' => 'Remove Bleat');

sub main(){
	# Build all of the Users' Data (Details & Bleats)
	Bitter::build_database();

	# Now tell CGI::Carp to embed any warning in HTML
    warningsToBrowser(1);
    
	handle_page();
}

# This function helps to handle the page and do the appropriate logic here
sub handle_page {
	my $user = get_cookie('user');
    my $session_id = get_cookie('auth_token');

	my $action = param('a'); # Command(s) / Action(s) to be made
    $action = Security::sanitize_data($action);

	if ($user && $session_id && Security::check_user($user)){ # Checks whether user is currently logged in or not. If not the page will be redirected to the home page.

		if ($action){
			handle_delete_bleat("$user") if ($action eq "db");
		}
		else{
			redirect_home();
		}
	}
	else{
		if ($action){
			handle_forget_password() if ($action eq "fp");
		}
		else{
			redirect_bitter();
		}
	}
}

# This function helps to handle forget password
sub handle_forget_password {
	my $email_username = param('rp_id') || "";
	my $recover_code = param('rec') || "";
	my $new_password = param('cpn') || "";

    $email_username = Security::sanitize_data($email_username);
    $recover_code = Security::sanitize_data($recover_code);
    $new_password = Security::sanitize_data($new_password);

	if (length($email_username) > 0 && length($recover_code) == 0 && length($new_password) == 0){
		if (Security::check_user($email_username)){
			$email_username = Bitter::get_username_by_email($email_username) if ($email_username =~ /\@/);

            my $url_link = $ENV{SCRIPT_URI};
            $url_link =~ s/\/$//;

			my $result = Bitter::recover_password("$email_username", "$url_link");

			if ($result){
				print_page("fp_success", "We just send you an email to you.<br/>Please recover your password through the link provided in the email!")
			}
			else{
				print_page("fp", "Fail to recover your password !");
			}
		}
		else{
			print_page("fp", "Invalid Username or Email Address !");
		}
	}
	elsif (length($email_username) > 0 && length($recover_code) > 0 && length($new_password) == 0){
		if (Security::check_user($email_username)){
			$email_username = Bitter::get_username_by_email($email_username) if ($email_username =~ /\@/);

			my $result = Bitter::change_password_verify("$email_username", "$recover_code");

			if ($result == 0){
				redirect_bitter();
			}
			elsif ($result == 2){
				print_page("fp_fail", "Recovery code is invalid. Please click the link in your email to activate your Account!");
			}
			else{
				print_page("cp", "", "$email_username", "$recover_code");
			}
		}
		else{
			redirect_bitter();
		}
	}
	elsif (length($email_username) > 0 && length($recover_code) > 0 && length($new_password) > 0){
		if (Security::check_user($email_username)){
			$email_username = Bitter::get_username_by_email($email_username) if ($email_username =~ /\@/);

			my $result = Bitter::change_password_recover("$email_username", "$recover_code", "$new_password");

			if ($result == 1){
				print_page("fp_success", "You have successfully changed your password!");
			}
			else{
				print_page("fp_fail", "Fail to change your password! Please try again later.");
			}
		}
		else{
			redirect_bitter();
		}
	}
	else{
		print_page("fp");
	}
}

# This function helps to handle deletion of bleat
sub handle_delete_bleat {
	my $user = $_[0];
	my $bleat_id = param('b_id') || "";
	my $should_delete = param('del') || "no";

	$bleat_id = Security::sanitize_data($bleat_id);
	$should_delete = Security::sanitize_data($should_delete);

	if (length($user) > 0 && length($bleat_id) > 0 && $should_delete eq "yes"){
		my $result = Bitter::delete_bleat("$user", "$bleat_id");

		if ($result){
			print_page("db_success", "You have successfully deleted your bleat!");
		}
		else{
			print_page("db_fail", "Fail to delete your bleat! Please try again later.");
		}
	}
	elsif (length($user) > 0 && length($bleat_id) > 0 && $should_delete ne "yes"){
		print_page("db", "","$user", "", "$bleat_id");
	}
	else{
		redirect_home();
	}
}

# This function helps to print out the appropriate action page
sub print_page{
    my $action 			= $_[0] || "";
    my $alert_message 	= $_[1] || "";
    my $username 		= $_[2] || "";
    my $recover_code	= $_[3] || "";
    my $bleat_id		= $_[4] || "";

    my $title_page = $page_title{"$action"};

    HTML::header("Bitter - $title_page");
    HTML::navigation("$title_page");
    
    if ($action eq "fp"){
    	HTML::build_recover_password("$alert_message");
	    HTML::footer(HTML::build_check_recover_password_script());
    }
    elsif ($action eq "fp_success" || $action eq "db_success"){
	    HTML::build_verification_page($alert_message, 0);
	    HTML::footer();
    }
	elsif ($action eq "fp_fail" || $action eq "db_fail"){
	    HTML::build_verification_page($alert_message, 1);
	    HTML::footer();
    }
    elsif ($action eq "cp"){ # Change Password
    	HTML::build_change_password_recovery("$username", "$recover_code");
    	HTML::footer(HTML::build_check_change_password_script());
    }
    elsif ($action eq "db"){
    	HTML::build_delete_bleat_page("$bleat_id");
	    HTML::footer();
    }
}

# This function helps to redirect user to the home page (not logged in)
sub redirect_bitter{
	print redirect(-url=>"./bitter.cgi");
	#HTML::redirect_page("./bitter.cgi");
}

# This function helps to redirect user to the home page (logged in)
sub redirect_home{
	print redirect(-url=>"./home.cgi");
	#HTML::redirect_page("./home.cgi");
}

# Get the cookie value given a key
sub get_cookie{
    my $key = $_[0];
    my $value = "";

    my %cookies = fetch CGI::Cookie;

    if ($cookies{"$key"}){
        $value = $cookies{"$key"}->value;
    }

    return $value;
}

main();

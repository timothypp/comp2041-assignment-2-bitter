#!/usr/bin/perl -w

#
# Written by Timothy Putra Pringgondhani (z5043683)
# z5043683@zmail.unsw.edu.au
#
# Created on 12 October 2015
# Last Update on 25 October 2015
#

# 
# Bitter uses several two external libraries or source code :
# 1. Bootstrap (CSS and Javascript)
# 2. JQuery 1.11.3 (Javascript + JQuery)
# 
# Bitter uses these libraries to allow a good UI to be made.
# The creator of Bitter acknowledges the work done by the people
# who made those libraries and does not acknowledge it is one of their own
# 

#
# This CGI file helps to display the home page. This home page is the landing page when the user is not logged in.
# This CGI file does small checking whether the appropriate information is filled in and
# sanitizes any information passed in.
#

use strict;

use CGI qw/:all/;
use CGI::Cookie;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

use Bitter;
use HTML;
use Security;

sub main(){
	# Build all of the Users' Data (Details & Bleats)
	Bitter::build_database();

	# Now tell CGI::Carp to embed any warning in HTML
    warningsToBrowser(1);
    
	handle_page();
}

# This function helps to handle the page and do the appropriate logic here
sub handle_page {
	my $user = get_cookie('user');
    my $session_id = get_cookie('auth_token');

    if ($user && $session_id && Security::check_user($user)){ # Checks whether user is currently logged in or not. If not the page will be redirected to the home page.
        print redirect(-url=>"./home.cgi");
        #HTML::redirect_page("./home.cgi");
    }
	else{
		HTML::header("Welcome to Bitter");
		HTML::navigation("Home");
		HTML::build_home_page();
        
        HTML::footer(HTML::build_check_login_script(), 
            HTML::build_check_sign_up_script());
	}
}

# Get the cookie value given a key
sub get_cookie{
    my $key = $_[0];
    my $value = "";

    my %cookies = fetch CGI::Cookie;

    if ($cookies{"$key"}){
        $value = $cookies{"$key"}->value;
    }

    return $value;
}

main();

# Bitter
> _COMP2041 Assignment 2 – Semester 2, 2015_

A social media website that mimics Twitter basic functionality.

Written in Perl with CGI framework.
With additional language such as HTML, CSS, Javascript.

### Live Demo
http://cgi.cse.unsw.edu.au/~z5043683/ass2/bitter.cgi

### Features
+ Basic Twitter Capability
+ Google API integration for location
+ Capable of sending email on friend request, mentions and new follower.

### Introduction
Andrew has noticed students are often complaining about COMP[29]041. Rather than address their complaints and improve COMP2041, Andrew has decided he will make himself rich exploiting COMP[29]041 coding skills and then give up lecturing. Andrew's plan is to have COMP[29]041 students create a social media platform called Bitter for complaints. Andrew believes the absence of any similar existing social media platform means Bitter will become very popular and he will become rich
Users using Bitter send short messages called bleats. Bleats can have at most maximum 142 characters.

Bitter users can indicate particular users they are interested in and whose bleats they wish to regularly see - this is termed listening to the user.

Your task is to produce a CGI script bitter.cgi which provides the core features of Bitter.

In other words your task is to implement a simple but fully functional social web site.

But don't panic, the assessment for this assignment (see below) will allow you to obtain a reasonable mark if you successfully complete some basic features.

### Subsets
+ Display User Information & Bleats (Level 0)
+ Interface (Level 0)
+ Logging In & Out (Level 1)
+ Search for Users By Username or Full Name (Level 1)
+ Sending Bleats (Level 1)
+ Displaying Relevant Bleats (Level 2)
+ Listening/Unlistening to Users (Level 2)
+ Searching Bleats (Level 2)
+ Replying to Bleat(Level 2)
+ Pagination of Bleats & Search Results (Level 3)
+ Bleat responses (Level 3)
+ User Account Creation (Level 3)
+ Profile Text (Level 3)
+ Password Recovery (Level 3)
+ Uploading & Deleting Images (Level 3)
+ Editing Infortmation (Level 3)
+ Deleting Bleats (Level 3)
+ Suspending/Deleting Bitter Account (Level 3)
+ Notifications (Level 3)
+ Bleets Attachment (Level 3)
+ Advanced Features (Level 4)
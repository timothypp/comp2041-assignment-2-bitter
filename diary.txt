commit 9a7dcda86558bfb074752909fb36e2902431d9b9
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 26 10:25:17 2015 +1100

    bug fixes

commit ba7a240d79f011bc50c0bf9b34e67a96af43bb6a
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 26 03:12:04 2015 +1100

    added back index.cgi

commit 129f48f687bfaf41b6da6bd16d1491409c2a5957
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 26 02:53:08 2015 +1100

    last update

commit 0a82350bdea3b95e9829b3ca010c78bf8fb0af85
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 26 02:48:26 2015 +1100

    new tar file

commit 414ca65a3302cae212b05ed99e10a90840afd1f0
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 26 02:47:39 2015 +1100

    last commit with bug fixes

commit ef33aedb96ed93eda08a265e39e4c304fc45e2f3
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 26 02:39:37 2015 +1100

    working attachment

commit b677a3514e534dd1cf3f44e262363a84875c1429
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 23:45:40 2015 +1100

    updated tar file

commit b949068a1b364a10638086a49e819034e49af784
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 23:33:54 2015 +1100

    final upload and with tar file for bitter

commit 3f28fda9aab08693b674c8b6a90476a008ea2335
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 19:14:05 2015 +1100

    test notify new followers

commit 680c490be533d35ed0e7ec0afa4bc7cd2d7bfe2a
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 19:00:28 2015 +1100

    testing notify reply

commit 012c5d6d36685bc4d3cfd238f5c909d2640a9b57
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 18:45:01 2015 +1100

    testing for notify mentioned bleat

commit b9f94c654832e4cedec08032474db6f12d94048a
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 17:09:33 2015 +1100

    small fix

commit fb74e2a2d5f16b5faeb090fc9ddf7479227b524c
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 16:59:56 2015 +1100

    updated css

commit c2ac2704a157edb3b300e8f336ab913fcab22f49
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 16:56:03 2015 +1100

    working suspend/unsuspend + delete account + bug fixes

commit 61e3407b6eabf0ce9ad6dff8e19bc9bcfe752f1e
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 12:32:21 2015 +1100

    fully working update information (inc. change password) + bug fixes

commit f5c44594589d18894960b90c498cea2b2617e8f9
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 12:00:01 2015 +1100

    small change

commit f7849410f6754065ba63be072ae8c536ebd0973f
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 11:56:04 2015 +1100

    updated diary

commit 2d90b37ef0579389d7ed174de803e60873c2f7c7
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 11:54:58 2015 +1100

    working change/delete profile pic and background image + bug fixes

commit e8b022a90e493ad27845ca2d70178278c701f4dd
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 11:19:54 2015 +1100

    bug fix

commit 37a6fe4094c1cbaf03bb08831711c43a17f51cd5
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 11:09:23 2015 +1100

    bug fix to upload image

commit a09e625cf1f0732bad781035e97e618ca76375bd
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 10:46:22 2015 +1100

    working recover password + upload-change profile pic + bug fix + reduce username limit to 16

commit cb2cdc3bf4a63341f338b9feb29b59916c9bc5b1
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 03:52:42 2015 +1100

    working recover password + bug fixes

commit 90ffe34394cfb598ccf5c1cba6b52dcfc1e51317
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 00:54:57 2015 +1100

    small fix

commit 38cd32be423c3b55d321431d25ea7ca71e206724
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 00:50:51 2015 +1100

    bug fix on recover password

commit 9026dd982f43a3e0d7329a28f178f66ab84f944b
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 00:39:39 2015 +1100

    fix sign up checking bug

commit b9cee1cfdf770daf4d8c945b17e971d57811ce8b
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 00:35:43 2015 +1100

    updated security module

commit 519623767e42e515b1b7690dca71cb2ccc46378d
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 25 00:30:48 2015 +1100

    almost done recover password + bug fixes + fix email message

commit 2246254f9c463764598a20b0387e6be69d525c59
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sat Oct 24 22:59:34 2015 +1100

    edit information (exc. pass) + profile text support + bug fixes to sign up and many more

commit 88cf6afc338eba4f9c1d53d3897307432a033921
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Fri Oct 23 14:39:28 2015 +1100

    working responses + prepare for change details + prepare for action.cgi + bug fixes + updated diary

commit d674eb3508f72273d446a26c0e6384b06f87d1dc
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Fri Oct 23 12:41:26 2015 +1100

    fix login problem using email

commit cb463bbdd9561e243d9e53110d0cf15c3c30968b
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Fri Oct 23 12:05:44 2015 +1100

    fix bug on sign up + login + use of relative url

commit c5c2e1c7bb0de9697ab27fedc3be6478de9753c0
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Thu Oct 22 23:34:44 2015 +1100

    fix on HTML.pm again

commit d476d02ac6ff97ea10309310c536b4732194efa4
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Thu Oct 22 23:33:53 2015 +1100

    fix on HTML.pm

commit efab0da201142f6fe442edd564720fa864d81157
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Thu Oct 22 23:28:20 2015 +1100

    small fix on home.cgi

commit cb7a3c017ba563313ac74312cc39de0889389959
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Thu Oct 22 23:25:20 2015 +1100

    small fix on home.cgi

commit f4c7633774b9ee074d3b8b87ab33fe4adb3b591d
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Thu Oct 22 23:21:56 2015 +1100

    bug fix on home screen for newly registerd user

commit 4adb6742e6af7c4a5ead5d216b263e1b70a31aff
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Thu Oct 22 23:13:04 2015 +1100

    fix bug on sign up activation

commit 7f48be34f905cfbfc79183e0d73e3d5cedb3cdbb
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Thu Oct 22 23:00:34 2015 +1100

    working sign up + verify + better login + fix md5 + bug fixes

commit d9cc48a3005402a28397de76713cb9de88be6ee4
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Thu Oct 22 19:41:41 2015 +1100

    changed md5 + bug fixes

commit 190e04b364ea84275f8a65a990e86e48308a2410
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Thu Oct 22 19:24:47 2015 +1100

    Pagination + Almost done sign up + bug fixes

commit 73514002d5cb121465734d1b55d5907e26c4a1a0
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 21 23:58:30 2015 +1100

    updated diary

commit d780a8d567551565979718412297aba679dd0f27
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 21 23:57:48 2015 +1100

    bug fix on new bleat + listening

commit 76e76343a406439394f702074e7bf3d623042d84
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 21 11:57:02 2015 +1100

    mention page + fix to search + bug fixes + diary

commit 09274777dcc4ffa122cf19f2b57f6864967ac0ca
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 19 23:30:16 2015 +1100

    bug fixes

commit d187c182d73f3a605a5c7f3e62d653e4b61331ad
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 19 23:23:05 2015 +1100

    update css

commit fce7acc8ddea17983da0f29b5d4e320cc24d15b4
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 19 23:19:37 2015 +1100

    bug fix on HTML

commit 4887fac1345f840cc170380d8e1926cce62eaec5
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 19 23:09:09 2015 +1100

    working reply bleat + support for non-letter words + bug fixes

commit 381ccaba7d6bf3aef2fbd1c10f354c4baa17a2f3
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 19 21:24:29 2015 +1100

    small bug fixes

commit 65cde0d4a9c9b356b01f23e091214b9d25716d66
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 19 21:19:53 2015 +1100

    working bleat search + bug fixes

commit 8568f4cdeb476d9f1f52838817610a78555a5ded
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 19 12:28:47 2015 +1100

    Bug Fixes

commit 343265e57c062c498d32368865495258b960c856
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 19 12:04:42 2015 +1100

    working relevant bleats + listen user (profile page only) + bug fixes

commit e9114e2e24f94c6ec48cd117ce672a9239066a70
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 18 23:34:02 2015 +1100

    working relevant bleats (home) + css update + bug fixes

commit 4a51526419f86b1a684ece881f841d1da4f912ab
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 18 19:25:04 2015 +1100

    bug fixes

commit 30cd7cb58ca15f66c2d00b150ad6dddad3ceb7a2
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 18 18:43:25 2015 +1100

    small update

commit c82eb719ba016755ed4bde7ae244f6236a0ba3d2
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 18 18:35:17 2015 +1100

    bug fix

commit 55acac923f3968ca66710595a0b563e71ef282aa
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 18 18:34:22 2015 +1100

    bug fix

commit 19b72b843f6266474ff98ea337acc2dda05b1a5e
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 18 18:27:52 2015 +1100

    working send bleat + javascript form checker + bug fixes + more security

commit 036ee6f11ad5aaa573e38ba98e777d31ad70729b
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Fri Oct 16 01:31:31 2015 +1100

    bug fix

commit 23744b74cb9f6bc2f0ecc150368d3e57d01647bf
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Fri Oct 16 01:29:29 2015 +1100

    missing css file

commit a75b3819d0c37a948c454d6d037a4cd01e5a8484
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Fri Oct 16 01:25:37 2015 +1100

    fix to last commit

commit 63a5a97b0e4d907742be98180f98da1b8b5fdd4b
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Fri Oct 16 01:24:13 2015 +1100

    implement bleat post page, md5 pass, working search, bug fixes

commit 0350eae40ebe09dce48b3fb94e6dd19f1606a6d1
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Thu Oct 15 21:04:15 2015 +1100

    bug fix + search capability

commit 8e16457f16ce86be5f2df551c025716217489571
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 14 01:26:52 2015 +1100

    bug fix

commit a0b0430806c1ba14328171b5d7cf3dd41915ff0e
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 14 01:23:50 2015 +1100

    wrong redirect

commit 63c9c2deac6d2834b2cad1a347adf48e18c1efa6
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 14 01:12:28 2015 +1100

    small fix

commit bd35472411bbea59bfa8ae68502dd3c509fdb9d7
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 14 01:11:03 2015 +1100

    small fix

commit 73b429f4fc94b01f04c8b145b4ec502d78f8b1b2
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 14 01:02:37 2015 +1100

    small fix

commit 260b44c56f9fed8136ec9478b0bab74617207c06
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 14 00:53:07 2015 +1100

    fix to redirect

commit bdfa8ad68a1dcc7c8e785f2e66e146b797927180
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 14 00:47:57 2015 +1100

    fix to index.cgi

commit 5a3fd7e43dc7811f1d49608f7c3ead2c3a29fd7e
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 14 00:42:43 2015 +1100

    bug fixes + working login + partial search + better ui + more cgi(s)

commit e2b50dfc52f28c377d0c3801ba571f353035b9fd
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 12 20:23:28 2015 +1100

    login implementation + bug fixes + new files

commit 90de9df295ae37dcc4bdf1afe80c4056bc13e2d8
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 12 02:38:19 2015 +1100

    small fix

commit 5f403094f0926633965e386954e9028f1c62a97e
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 12 02:35:50 2015 +1100

    added about page

commit 79ae7bff56ca80c96c09e8666887d0ab26a74000
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 12 02:33:03 2015 +1100

    bug fix

commit 683b6fc66103e1192852c7d9ddd826af3c8c8523
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 12 02:30:16 2015 +1100

    dynamic background fix

commit 8c5a009b330b35ce38d8e2433adee4270159bba9
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Mon Oct 12 02:15:19 2015 +1100

    bug fix + dynamic background + signup page

commit 58a9f7b5252f7cae92a31c2ee1826fb45503f3ce
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 11 19:21:36 2015 +1100

    added signup.cgi

commit bae3844d67c09a202098decbd4cc5621f757c5ef
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 11 19:21:08 2015 +1100

    some bug fixes

commit c038053cebe10ce0e852764568c3c70acd63f5b3
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sun Oct 11 17:31:14 2015 +1100

    preparing for login + bug fixes

commit 51e1b437beab13b115e55919e20e1fec35e4d4ef
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sat Oct 10 02:42:04 2015 +1100

    bug fix

commit d84080cde896501cb7f8517d865747d2251b05a3
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sat Oct 10 02:33:38 2015 +1100

    bitter now displays bleats in profile page

commit 5711c582f92b3b8c778bff1ab986d3f512cac30f
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Sat Oct 10 00:15:17 2015 +1100

    handled user profile page without bleats being displayed

commit 3336add6413e84db7863a6ec87bbd9dcb131f82e
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Fri Oct 9 19:06:26 2015 +1100

    bug fix

commit 9ef4f7d94706bb6788bfc8493102d0545bce78c2
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Fri Oct 9 19:02:08 2015 +1100

    added not found page + some bug fixes

commit f3cf6419dc1431210321b1cea452bc332f4ca888
Author: Timothy Putra Pringgondhani <z5043683@cse.unsw.EDU.AU>
Date:   Wed Oct 7 08:07:12 2015 +1100

    bug fix

commit 444bec1ec902491366b5eb0a1eaac9a4257f6cf5
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 7 00:42:52 2015 +1100

    small fix

commit 006ceb9643b79d73786233714df2f3853586c221
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 7 00:41:51 2015 +1100

    small fix

commit 83a2030c6fd75d30856bc81b67b92a4fab6d0aa0
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 7 00:38:17 2015 +1100

    small fix

commit 3b01265c45982bc452134e3b190723b17c74b078
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 7 00:35:33 2015 +1100

    dataset-medium

commit 1667af116d143cd019451f4cf61bd4c3056faafa
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Wed Oct 7 00:30:06 2015 +1100

    added some HTML related stuff

commit 5b793f447acd8efc921bcad15eb86b64d3712eb1
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Tue Oct 6 19:01:44 2015 +1100

    added some files

commit 2f82738951d1102253abcb16848c98d17cdf7e5a
Author: Timothy <Timothy@Timothys-MacBook-Air.local>
Date:   Tue Oct 6 19:00:08 2015 +1100

    started working on ass2 with pm

commit beb0007f97b4b52687c9ec9f33152f7727390120
Author: Timothy Putra Pringgondhani <z5043683@cse.unsw.EDU.AU>
Date:   Fri Oct 2 01:10:03 2015 +1000

    Initial Code

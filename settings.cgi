#!/usr/bin/perl -w

#
# Written by Timothy Putra Pringgondhani (z5043683)
# z5043683@zmail.unsw.edu.au
#
# Created on 12 October 2015
# Last Update on 25 October 2015
#

#
# This CGI file helps to display the account settings page.
# This CGI file does small checking whether the appropriate information is filled in and
# sanitizes any information passed in.
#

use strict;

use CGI qw/:all/;
use CGI::Cookie;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

use Bitter;
use HTML;
use Security;

sub main(){
    # Build all of the Users' Data (Details & Bleats)
    Bitter::build_database();

    # Now tell CGI::Carp to embed any warning in HTML
    warningsToBrowser(1);
    
    handle_page();
}

# This function helps to handle the page and do the appropriate logic here
sub handle_page {
    my $user = get_cookie('user');
    my $session_id = get_cookie('auth_token');

    if ($user && $session_id && Security::check_user($user)){ # Checks whether user is currently logged in or not. If not the page will be redirected to the home page.
        my $action      = param('act') || "";

        my $alert_message_change_info = "";
        my $alert_message_change_pass = "";

        my $alert_message_change_pp = "";
        my $alert_message_change_bg = "";

        my $alert_message_change_acc = "";
 
        if ($action eq "c_acci") {# Change Account Information
            my $email       = param('ac_em') || "";
            my $full_name   = param('fn') || "";
            my $suburb      = param('sub') || "";
            my $latitude    = param('lat') || "";
            my $longitude   = param('long') || "";
            my $prof_text   = param('pt') || "";


            $email = Security::sanitize_data($email);
            $full_name = Security::sanitize_data($full_name);
            $suburb = Security::sanitize_data($suburb);

            $latitude = Security::sanitize_data($latitude);
            $longitude = Security::sanitize_data($longitude);
            $prof_text = Security::sanitize_non_safe_html_tags($prof_text);

            if($email !~ /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/ && length($email) > 0) {
                $alert_message_change_info = "Please enter a valid Email Address !";
            }
            elsif ($latitude !~ /^(-)?[0-9.]+$/ && length($latitude) > 0){
                $alert_message_change_info = "You have entered an invalid Location. Please enter the correct Latitude !";
            }
            elsif ($longitude !~ /^(-)?[0-9.]+$/ && length($longitude) > 0){
                $alert_message_change_info = "You have entered an invalid Location. Please enter the correct Longitude !";
            }

            if (not $alert_message_change_info){
                my $result = Bitter::update_account_information($user, $email, $full_name, $suburb, $latitude, $longitude, $prof_text);

                if ($result == 0){
                    $alert_message_change_info = "The Email Address has been used before, please choose another Email Address !";
                }
                elsif ($result == 2){
                    $alert_message_change_info = "An error has occurred when updating your information !";
                }
                else{
                    $alert_message_change_info = "Successfully changed your profile information !";
                }
            }
        }
        elsif ($action eq "c_pass") { # Change Account Password
            my $old_password    = param('cpo') || "";
            my $new_password    = param('cpn') || "";
            my $verify_password = param('cpnv') || "";

            $old_password = Security::sanitize_data($old_password);
            $new_password = Security::sanitize_data($new_password);
            $verify_password = Security::sanitize_data($verify_password);

            my $result = Security::login_helper("$user", "$old_password");

            if ($result == 1){
                if ("$new_password" eq "$verify_password"){
                    my $result = Bitter::change_password("$user", "$new_password");

                    if ($result == 1){
                        $alert_message_change_pass = "You have successfully changed your password!";
                    }
                    else{
                        $alert_message_change_pass = "Fail to change your password! Please try again later.";
                    }
                }
                else{
                    $alert_message_change_pass = "The new Password does not match the Verify new Password.";
                }
            }
            else{
                $alert_message_change_pass = "The Old Password that you entered did not match our records. Please double-check and try again.";
            }
        }
	    elsif ($action eq "c_img_pp"){ # Change Profile Picture Image
	    	my $uploaded_image = upload('new_img_profile');

			if (defined $uploaded_image) {
			    $user = Bitter::remove_new_line_and_spaces($user);
			    $user = lc($user);

			    my $users_dir = $Bitter::users_dir;
                my $folder_name = $Bitter::global_all_users{"$user"}{"username"};
                my $image_location = "$users_dir/$folder_name/profile.jpg";

		        if (-f $image_location){ # Check if profile image does exists
					unlink $image_location or return;
				}

		        open(my $cpp,'>',$image_location) or return;
		        binmode $cpp;

		        while (my $line = <$uploaded_image>) {
		            print $cpp $line;
		        }

		        close $cpp;

		        my $result = Bitter::change_profile_image("$user");
		        
                if ($result){
                    $alert_message_change_pp = "Successfully changed your Profile Picture !";
                }
                else{
                    $alert_message_change_pp = "Fail to change your Profile Picture !";
                }
		    }
	    }
	    elsif ($action eq "d_img_pp") { # Delete Profile Picture Image
	    	my $result = Bitter::delete_profile_image("$user");

            if ($result){
    	    	$alert_message_change_pp = "Successfully deleted your Profile Picture !";
            }
            else{
                $alert_message_change_pp = "Fail to delete your Profile Picture !";
            }
	    }
        elsif ($action eq "c_img_bg"){ # Change Background Image
            my $uploaded_image = upload('new_img_back');

            if (defined $uploaded_image) {
                $user = Bitter::remove_new_line_and_spaces($user);
                $user = lc($user);

                my $users_dir = $Bitter::users_dir;
                my $folder_name = $Bitter::global_all_users{"$user"}{"username"};
                my $image_location = "$users_dir/$folder_name/background.jpg";

                if (-f $image_location){ # Check if profile image does exists
                    unlink $image_location or return;
                }

                open(my $cpp,'>',$image_location) or return;
                binmode $cpp;

                while (my $line = <$uploaded_image>) {
                    print $cpp $line;
                }

                close $cpp;

                my $result = Bitter::change_background_image("$user");
                
                if ($result){
                    $alert_message_change_bg = "Successfully changed your Background Image !";
                }
                else{
                    $alert_message_change_bg = "Fail to change your Background Image !";
                }
            }
        }
        elsif ($action eq "d_img_bg") { # Delete Background Image
            my $result = Bitter::delete_background_image("$user");

            if ($result){
                $alert_message_change_bg = "Successfully deleted your Background Image !";
            }
            else{
                $alert_message_change_bg = "Fail to delete your Background Image !";
            }
        }
        elsif ($action eq "suspend") { # Suspend Account
            my $result = Bitter::suspend_unsuspend_account("$user", 1);

            if ($result){
                $alert_message_change_acc = "Successfully suspended your Account !";
            }
            else{
                $alert_message_change_acc = "Fail to suspend your Account !";
            }
        }
        elsif ($action eq "unsuspend"){ # Unsuspend Account
            my $result = Bitter::suspend_unsuspend_account("$user", 0);

            if ($result){
                $alert_message_change_acc = "Successfully unsuspended your Account !";
            }
            else{
                $alert_message_change_acc = "Fail to unsuspend your Account !";
            }
        }
        elsif ($action eq "delete") { # Delete Account
            my $result = Bitter::delete_account("$user");

            if ($result){
                print redirect(-url=>"./logout.cgi");
                #HTML::redirect_page("./logout.cgi");
            }
            else{
                $alert_message_change_acc = "Fail to delete your Account !";
            }
        }
        elsif ($action eq "c_notif"){
            my $notify_mention      = param('notify_mention') || "off";
            my $notify_reply        = param('notify_reply') || "off";
            my $notify_new_listen   = param('notify_new_listen') || "off";

            $notify_mention = Security::sanitize_data($notify_mention);
            $notify_reply = Security::sanitize_data($notify_reply);
            $notify_new_listen = Security::sanitize_data($notify_new_listen);

            Bitter::nofication_update($user, $notify_mention, $notify_reply, $notify_new_listen);
        }


        HTML::header("Bitter - Account Settings");
        HTML::navigation("Settings");

	    Bitter::get_account_information($user, $alert_message_change_info, $alert_message_change_pass, $alert_message_change_pp, 
            $alert_message_change_bg, $alert_message_change_acc);

        HTML::footer(HTML::build_check_update_info_script(), HTML::build_check_update_password_script(), 
            HTML::build_get_location_script());
    }
    else{
        print redirect(-url=>"./bitter.cgi");
        #HTML::redirect_page("./bitter.cgi");
    }
}

# Get the cookie value given a key
sub get_cookie{
    my $key = $_[0];
    my $value = "";

    my %cookies = fetch CGI::Cookie;

    if ($cookies{"$key"}){
        $value = $cookies{"$key"}->value;
    }

    return $value;
}

main();

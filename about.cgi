#!/usr/bin/perl -w

#
# Written by Timothy Putra Pringgondhani (z5043683)
# z5043683@zmail.unsw.edu.au
#
# Created on 12 October 2015
# Last Update on 25 October 2015
#

#
# This CGI file helps to display the about page.
# This CGI file does small checking whether the appropriate information is filled in and
# sanitizes any information passed in.
#

use strict;

use CGI qw/:all/;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

use HTML;
use Security;

sub main(){
	HTML::header("Bitter - About");

	# Now tell CGI::Carp to embed any warning in HTML
    warningsToBrowser(1);
    
    HTML::navigation("About");
    HTML::build_about_page();
    HTML::footer();
}

main();

#!/usr/bin/perl -w

#
# Written by Timothy Putra Pringgondhani (z5043683)
# z5043683@zmail.unsw.edu.au
#
# Created on 12 October 2015
# Last Update on 24 October 2015 11:52 PM
#

# This Perl Module is designed to provide security or act as a security module
# for Bitter.
#
# This module helps to sanitize information when they are being passed in through param(s).
# With sanitizing, it will help to prevent XSS from happening.
#
# This module also helps to allow certain saft HTML tags to be displayed. This is useful
# when displaying information such as user's description.
#

use strict;

use Digest::MD5; # Call MD5 Digest for storing hashed password
use HTML::Entities; # Sanitizes Information (Removing capability of XSS)
use HTML::Scrubber; # Sanitizes Data. Sanitizes the data but allowing certain HTML tags to be displayed

use Bitter; # User Bitter package so that it can call Bitter.pm function(s)

package Security;
 
my @allow = qw[ ul li ol p br hr b u a i pre blockquote tt dl dd dt br ]; # List of allowable HTML tags

my @rules = ( # Setting up rules which set what is allowed in the html tags
	 script => 0,
	 img => {
	 src => qr{^(http://)}i,   # only absolute image links allowed
	 alt => 1,                 # alt attribute allowed
	 '*' => 0,                 # deny all other attributes
	 },
	 a => {
	 href => 1,                # HREF
	 title => 1,               # ALT attribute allowed
	 rel   => 1,               # Link relationship 
	 '*' => 0,                 # deny all other attributes
	 },
);

my @default = (
   0   =>    # default rule, deny all tags
   {
   '*'           => 1, # default rule, allow all attributes
   'href'        => qr{^(?!(?:java)?script)}i,
   'src'         => qr{^(?!(?:java)?script)}i,
   'cite'        => '(?i-xsm:^(?!(?:java)?script))',
   'language'    => 0,
   'name'        => 1, # could be sneaky, but hey ;)
   'onblur'      => 0,
   'onchange'    => 0,
   'onclick'     => 0,
   'ondblclick'  => 0,
   'onerror'     => 0,
   'onfocus'     => 0,
   'onkeydown'   => 0,
   'onkeypress'  => 0,
   'onkeyup'     => 0,
   'onload'      => 0,
   'onmousedown' => 0,
   'onmousemove' => 0,
   'onmouseout'  => 0,
   'onmouseover' => 0,
   'onmouseup'   => 0,
   'onreset'     => 0,
   'onselect'    => 0,
   'onsubmit'    => 0,
   'onunload'    => 0,
   'src'         => 0,
   'type'        => 0,
   }
);

# This function helps to check whether the given username is valid (exists in the databse or it is not).
# This function acts as a bridge with the CGI script and Bitter.pm
sub check_user{
	my $login_id = $_[0];

	if (check_valid_username($login_id)){ # If the username is valid (it does not contain any unallowed character) then it will check whether the usernam exists
	    return Bitter::check_username_email($login_id); # 
	}

	return 0;
}

# This function helps to check whether the given username or email and password matches the database.
# This function acts as a bridge with the CGI script and Bitter.pm
sub login_helper{
	my $login_id = $_[0];
	my $password = $_[1];

	if (check_valid_username($login_id)){ # If the username is valid (it does not contain any unallowed character) then it will check whether the usernam exists.
		my $result = Bitter::check_login_id_password($login_id, $password);
		return $result;
	}

	return 0;
}

# Checks whether the username or email valid. Valid username or email only allows alphanumeric characters, dash, dot, at or underscore.
sub check_valid_username{
	my $login_id = $_[0];

	if ($login_id =~ /^[A-Za-z0-9\_\-\.\@]+$/) {
		return 1;
	}

	return 0;
}

# This function helps to sanitize data by encode characters that can do XSS to HTML entities.
sub sanitize_data {
	my $data = $_[0];
	$data =~ s/^\s+//;
	$data =~ s/\s+$//;
	
	$data = HTML::Entities::encode_entities($data, '<>&"'); # The given data will encode characters such as <, >, &, " to prevent XSS.

	return $data;
}

# This function helps to sanitizes information given but at the same time allows certain HTML tags to be displayed. (List of allowable tags is listed above).
sub sanitize_non_safe_html_tags {
  my $data = $_[0];
  $data =~ s/^\s+//;
  $data =~ s/\s+$//;

  my $safe = HTML::Scrubber->new();
  $safe->allow( @allow );
  $safe->rules( @rules );
  $safe->default( @default );

  # deny HTML Comments
  $safe->comment(0);

	$data = $safe->scrub( $data );

	return $data;
}

# This function helps to encode the given information into a MD5 Hex so that it is not easily readable
sub encode_data_md5{ # Added Measured Security so that value is not readable (e.g. password)
	my $data = $_[0];

	my $ctx = Digest::MD5->new;
	$ctx->add($data);
	$data = $ctx->hexdigest;

	return $data;
}

1;
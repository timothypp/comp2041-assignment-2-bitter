#!/usr/bin/perl -w

#
# Written by Timothy Putra Pringgondhani (z5043683)
# z5043683@zmail.unsw.edu.au
#
# Created on 12 October 2015
# Last Update on 25 October 2015 
#

#
# This CGI file helps to display the profile page.
# This CGI file does small checking whether the appropriate information is filled in and
# sanitizes any information passed in.
#

use strict;

use CGI qw/:all/;
use CGI::Cookie;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

use Bitter;
use HTML;
use Security;

sub main(){
	# Build all of the Users' Data (Details & Bleats)
	Bitter::build_database();

	# Now tell CGI::Carp to embed any warning in HTML
    warningsToBrowser(1);
    
	handle_page();
}

# This function helps to handle the page and do the appropriate logic here
sub handle_page {
	my $user = get_cookie('user');
    my $session_id = get_cookie('auth_token');

	if ($user && $session_id && Security::check_user($user)){ # Checks whether user is currently logged in or not. If not the page will be redirected to the home page.
		my $user_search = param('user') || "";
		my $follow = param('follow') || "";
        my $page_number = param('n') || 1;

        $user_search =~ substr(quotemeta $user_search, 0, 16); # Substring login id to 16 characters
        $user_search = Security::sanitize_data($user_search);

		if (length($user_search) > 0 && length($follow) > 0){
			if ($follow eq "yes"){
				Bitter::add_remove_following($user, $user_search, 1);	
			}
			elsif ( $follow eq "no"){
				Bitter::add_remove_following($user, $user_search, 0);	
			}
		}

		if (length($user_search) == 0){
			$user_search = $user;
		}

		Bitter::get_user_profile("$user_search", "$page_number");
	}
	else{
		print redirect(-url=>"./bitter.cgi");
		#HTML::redirect_page("./bitter.cgi");
	}
}

# Get the cookie value given a key
sub get_cookie{
    my $key = $_[0];
    my $value = "";

    my %cookies = fetch CGI::Cookie;

    if ($cookies{"$key"}){
        $value = $cookies{"$key"}->value;
    }

    return $value;
}

main();

#!/usr/bin/perl -w

#
# Written by Timothy Putra Pringgondhani (z5043683)
# z5043683@zmail.unsw.edu.au
#
# Created on 15 October 2015
# Last Update on 25 October 2015
#

#
# This CGI file helps to display the write new bleat page.
# This CGI file does small checking whether the appropriate information is filled in and
# sanitizes any information passed in.
#

use strict;

use CGI qw/:all/;
use CGI::Cookie;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

use File::Basename; # Use to get the filename, path and extension

use HTML;
use Security;

sub main(){
    # Build all of the Users' Data (Details & Bleats)
    Bitter::build_database();

    # Now tell CGI::Carp to embed any warning in HTML
    warningsToBrowser(1);
    
    handle_page();
}

# This function helps to handle the page and do the appropriate logic here
sub handle_page{
    my $user = get_cookie('user');
    my $session_id = get_cookie('auth_token');

    if ($user && $session_id && Security::check_user($user)){ # Checks whether user is currently logged in or not. If not the page will be redirected to the home page.
        my $bleat_message = param('bm') || "";
        my $bleat_latitude = param('lat') || "";
        my $bleat_longitude = param('long') || "";
        my $checked_location = param('cl') || "";
        my $reply_to = param('rt') || "";

        $bleat_message =~ substr(quotemeta $bleat_message, 0, 142); # Substring login id to 142 characters
        $bleat_message = Security::sanitize_data($bleat_message);

        $bleat_latitude = Security::sanitize_data($bleat_latitude);
        $bleat_longitude = Security::sanitize_data($bleat_longitude);

        my $message = "";

        if (length($bleat_message) > 0){
            
            if ($checked_location ne "on"){
                $bleat_latitude = "";
                $bleat_longitude = "";
            }

            my $result = Bitter::new_bleat($user, $bleat_message, $bleat_latitude, $bleat_longitude, $reply_to); # This returns the bleat ID that is used for the post

            if ($result){
                upload_file($result);

                $message = "Bitter successfully posted your Bleat !";
                ($bleat_message, $bleat_latitude, $bleat_longitude) = ("") x3;
            }
            else{
                $message = "Bitter failed to post your Bleat !";
            }
        }

        HTML::header("Bitter - Write Bleat");
        HTML::navigation("Write");
        HTML::build_write($reply_to, $message, $bleat_message, $bleat_latitude, $bleat_longitude);
        
        HTML::footer(HTML::build_write_count_script(), 
            HTML::build_check_write_script(), 
            HTML::build_get_location_script());
    }
    else{
        print redirect(-url=>"./bitter.cgi");
        #HTML::redirect_page("./bitter.cgi");
    }
}

sub upload_file {
    my $bleat_id = $_[0];

    my $file_one = upload('bleat_up_1');
    my $file_two = upload('bleat_up_2');
    my $file_three = upload('bleat_up_3');
    my $file_four = upload('bleat_up_4');

    my $upload_dir = $Bitter::upload_dir;

    if (defined $file_one) {
        my ( $name, $path, $extension ) = fileparse ( $file_one, qr"\..[^.]*$");
        my $file_location = "$upload_dir/$bleat_id" . "_" . "1" . "$extension";

        open(my $cpp,'>',$file_location) or return;
        binmode $cpp;
        while (my $line = <$file_one>) {
            print $cpp $line;
        }

        close $cpp;
        chmod 0755, $file_location;
    }
    
    if (defined $file_two) {
        my ( $name, $path, $extension ) = fileparse ( $file_two, qr"\..[^.]*$");
        my $file_location = "$upload_dir/$bleat_id" . "_" . "2" . "$extension";

        open(my $cpp,'>',$file_location) or return;
        binmode $cpp;
        while (my $line = <$file_two>) {
            print $cpp $line;
        }

        close $cpp;
        chmod 0755, $file_location;
    }
    
    if (defined $file_three) {
        my ( $name, $path, $extension ) = fileparse ( $file_three, qr"\..[^.]*$");
        my $file_location = "$upload_dir/$bleat_id" . "_" . "3" . "$extension";

        open(my $cpp,'>',$file_location) or return;
        binmode $cpp;
        while (my $line = <$file_three>) {
            print $cpp $line;
        }

        close $cpp;
        chmod 0755, $file_location;
    }

    if (defined $file_four) {
        my ( $name, $path, $extension ) = fileparse ( $file_four, qr"\..[^.]*$");
        my $file_location = "$upload_dir/$bleat_id" . "_" . "4" . "$extension";

        open(my $cpp,'>',$file_location) or return;
        binmode $cpp;
        while (my $line = <$file_four>) {
            print $cpp $line;
        }

        close $cpp;
        chmod 0755, $file_location;
    }

    return 1;
}

# Get the cookie value given a key
sub get_cookie{
    my $key = $_[0];
    my $value = "";

    my %cookies = fetch CGI::Cookie;

    if ($cookies{"$key"}){
        $value = $cookies{"$key"}->value;
    }

    return $value;
} 

main();
